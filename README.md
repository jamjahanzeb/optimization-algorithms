This project contains a small toolkit that can use 6 optimization techniques: Evolutionary Algorithm, Evolution Strategy, Evolutionary Programming, Artificial Immune System, Particle Swarm Optimization and Neural Networks. The basic purpose of this toolkit is to present a comparison between different optimization techniques that are used: hence, for the time being,  it's restricted to only 2-3 functions for the earlier 5 mentioned and only to a set of defined inputs in case of Neural Networks. The sample inputs for the neural network file are also attached with this project for the user's convenience.


This toolkit was designed in NetBeans 8.0.2 and uses the open source libraries JCommon and JFreeCharts, it's requested that you install the above mentioned for this toolkit to work efficiently. Although installation guide for JCommon and JFreeCharts libraries is also attached with this project, I still urge you to go to their official website and follow the steps mentioned their to avoid any inconvenience.


Further additions will be added to this toolkit and updated in the future.